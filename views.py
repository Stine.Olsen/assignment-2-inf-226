
from json import dumps

import flask
from apsw import Error
from flask import (abort, make_response, render_template, request,
                   send_from_directory)
from flask_login import current_user, login_required, login_user,logout_user
from markupsafe import escape

from app import app, conn
from login_form import LoginForm
from pygmentize import cssData, pygmentize
from user import user_loader,check_password



@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')



@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return render_template('./index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        u = conn.execute (f"SELECT * FROM users WHERE username = ? ",[username])
        if u :
            db_password = conn.execute(f"SELECT password FROM users WHERE username = ?",[username]).fetchone()[0]
        if u and check_password(password,db_password):
            user = user_loader(username)
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index_html'))
    return render_template('./login.html', form=form)





@app.get('/search')
def search():
    user = current_user.id
    query = request.args.get('q') or request.form.get('q') or '*'

    
    try:
        if query == "*":
            stmt = f"SELECT * FROM messages WHERE sender = ?"
            result = f"Query: {pygmentize(f' {stmt[:-1]} {user}')} \n"
            c = conn.cursor().execute(stmt, [user])
        rows = c.fetchall()
        result = result + 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET'])
def send():

    try:
        sender = current_user.id
        message = request.args.get('message') or request.args.get('message')
        if not sender or not message:
            return f'ERROR: missing sender or message'
        stmt = f"INSERT INTO messages (sender, message) values (?, ?);"
        result = f"Query: {pygmentize(f'{stmt[:-7]} ( {sender} , {message} )')}\n"

        conn.execute(stmt,(sender,message))
        return f'{result} Message sendt'
    except Error as e:
        return f'{result}ERROR: {e}'

@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}


@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp

