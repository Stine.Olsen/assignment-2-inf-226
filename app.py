import sys

import apsw
from apsw import Error
from flask import Flask
from bcrypt import gensalt, hashpw
import secrets
conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = secrets.token_bytes(32)

# Add a login manager to the app
import flask_login

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        message TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')

    c.execute('''CREATE TABLE IF NOT EXISTS users (
        id integer PRIMARY KEY, 
        username TEXT NOT NULL,
        password TEXT NOT NULL); ''')

    # Add sample users if user table is empty
    if c.execute('SELECT COUNT(*) FROM users').fetchone()[0] == 0:
       c.execute(f'INSERT INTO users (username, password) VALUES ("bob", "{hashpw("bob".encode("utf-8"), gensalt()).decode("utf-8")}")')
       c.execute(f'INSERT INTO users (username, password) VALUES ("alice", "{hashpw("passord123".encode("utf-8"), gensalt()).decode("utf-8")}")')

except Error as e:
    print(e)
    sys.exit(1)

import views
