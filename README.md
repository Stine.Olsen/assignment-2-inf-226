# Assignment 2A +2B

#### Stine Olsen

## 2A

### Things i have notised

#### In the code

- In the file "app.py," usernames and passwords are kept in plaintext in a dictionary. and new users can only be created by manually adding them to the dictionary in "app.py."

* One of the code's many problems is that it doesn't follow Flask's "recommended" project structure configuration. because of this other developers will find it challenging to, for example, understand the relationship between files, change the structure, or check for security problems as a result. Additionally, it will be quite challenging to expand the project with new implementations.

- There were alot of unused code, and code that seemd not to be relatet to this application.

#### on the website

- First, a legitimate username serves as the only requirement for logging in on the login screen.

* A user can sign in as, say, Alice yet send messages as Bob.
  Nameley everyone can write in eachothers name.

- Messages could be sent to users who don't exist.

* Whenever you conduct a search, the query that was used to retrieve the results is printed on the webpage. Facilitating others' understanding of the service database's internal structure.

### 2B

#### _Design considerations_

Write a README.md with:

- a brief overview of your design considerations from Part A,

#### _Features in this application_

- Database with hashed and salted passwords.

* A login that checks the password corresponding to the username.

- when searchin with `*` a user only sees the messages they them self has sendt.

* Logout button, in top left corner, that redirects to the login page

#### _Instructions on how to test/demo it_

Bcrypt installation is required to use this program. Run this command in your terminal to install:
`pip install bcrypt `

Start the application by using the command "flask run." After that, visit the website at "localhost:5000" to be taken to the login screen.

There are already two users, "alice" and "bob", with the respective passwords "password123" and "bob," when the application is launched.

#### _Technical details on the implementation_

Password hashing is done using the bcrypt library.
flask_login has been used to handle login and logout.
[Secrets.token_bytes](https://docs.python.org/3/library/secrets.html) has been used for randomizing the app.secret_key, and was set to 32 bits as it is believed to be sufficient for the majority of usage cases.

### Threat model – who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?

In this application there could be several different types of attacers. Some people desire to read messages delivered within the program or extract data from the database in order to potentially unearth sensitive or confidential information. Data that is already present in the program may be changed by some attackers. The program may be stopped partially or completely. Posing a threat to all three CIA triad components.

**SQL injections**
The "search" feature is vulnerable to SQL injections. This could enable attackers to download the entire database (confidentiality). Additionally, they have the option of editing or erasing data (integrity). The program may be stopped partially or completely (availability). Violating all three CIA triangle.

### What are the main attack vectors for the template application?

The fundamental problem with the existing application is that it doesn't require users to use secure passwords; as things stand, this leaves users open to attack. When the user is authorized, this could result in a CSRF (Cross-Site Request Forgery). When a person logs in, the application now redirects them to a different website, but it does not examine the URL they are being forwarded to. Consequently, a chance to profit from this weakness is created.

### What should we do (or what have you done) to protect against attacks?

First, whenever a user input is entered in a query, I use prepared statements to protect against sql injection.

Passwords are now stored in the database using hashes rather than plaintext. Additionally, to avoid rainbow table attacks password, salt was added on the hashed passwords.

In order to prevent impersonation, logging in now requires a password.

### What is the access control model?

Due to the problem with user authentication, the program actually lacks access controls.he program's intended access control is mandatory access control, in which users should be granted authorization to perform any action within the application, but visitors without a user account are not given access.

### How can you know that you security is good enough? (traceability)

A "good" place to start is asking if there are steps taken to mitagte and reduse the attack surface of the most common threats. Since it is challenging to cover all bases, tracability — possibly in the form of application logging—is essential for identifying the issue and the context in which it occurred. It is simpler to prevent such attacks from happening again if you can first identify them.

# TASK 3

##### Do you understand the overall design, based on the documentation and available source code?

The application's features and constraints were made apparent in the documentation.
I apprecate the partitotion into `routes.py ` `db_setup.py `

##### Do you see any potential improvements that could be made in design / coding style / best practices? I.e., to make the source code clearer and make it easier to spot bugs.

The decision to have a separate `routs.py` file for handling routes was a sensible one, although not all routes are in this file; login is managed in `app.py` .Having a `routes.py` file establishes the expectation that every route will be contained there, and anything missing will be interpreted as nonexistent.

Unused code should have been deleted, even if it is commented out.

In `app.py`, for instance, there are numerous imports that are not used and may occasionally result in conflicts between classes in the namespace.

The project structure might yet be improved; for example, the index.html could be placed under the template folder. Additionally, an assets/static folder could have contained.png and.ico files for better organization.

The forms `login_form.py ` and`register_form.py` could have been in their own folder or in a single file.

##### Based on the threat model described by the author(s) (from part 2B), do you see any vulnerabilities in the application?

XSS, also metioned by the auther, is a voulnerability.
A benign example sending the message :
` print <h1> TACO </h1>`
result in the layout of the messag to change into a title.

##### Do you see any security risks that the author(s) haven't considered? (Have a look at the OWASP Top Ten list and “Unit 2: Attack Vectors” in Security for Software Engineers)

A06:2021-Vulnerable and Outdated Components -> some unused functions have not been removed (_favicon_ico_ and _favicon_png_).

A09:2021-Security Logging and Monitoring Failures -> No logging outside of logged in user and messages.
