from base64 import b64decode
import flask
from flask_login import current_user,login_required,logout_user

from app import app,conn, flask_login, login_manager

from bcrypt import checkpw

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass


# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    user = conn.execute(f'SELECT COUNT(*) FROM users WHERE username = ?',[user_id] )
    if user == 0:
        return

    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    return user


@app.route( '/logout' ,methods = ['GET','POST']) 
@login_required
def logout():
    logout_user()
    return flask.redirect(flask.url_for('login'))



@app.route('/getUsername')
@login_required
def username():
    return current_user.id

@app.get('/check_password/')
def check_password(input_pass, db_hash):
    return checkpw(input_pass.encode("utf-8"), db_hash.encode("utf-8"))

