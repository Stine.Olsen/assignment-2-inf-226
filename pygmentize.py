from threading import local

from pygments import highlight, token
from pygments.filters import KeywordCaseFilter, NameHighlightFilter
from pygments.formatters import HtmlFormatter
from pygments.lexers import SqlLexer

tls = local()
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'
